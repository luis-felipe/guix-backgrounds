Guix Backgrounds
================

The files in this directory are intended to be used as backgrounds
for different components of the GNU operating system, such as login
managers and desktop environments. The backgrounds are available in
different aspect ratios which are indicated in the file name (for
example, "waves-16-9.svg" has an aspect ratio of 16:9).


Copying
-------

Unless otherwise stated, all the files in this repository are placed
in the public domain with the following note:

  Public domain 2015 Luis Felipe López Acevedo. All rights waived.

The `Guix logo`_ (⋎) is licensed under the following terms:

  Copyright © 2015 Luis Felipe López Acevedo

  Permission is granted to copy, distribute and/or modify this work
  under the terms of the Creative Commons Attribution-ShareAlike 4.0
  International License.


.. LINKS
.. _Guix logo: https://luis-felipe.gitlab.io/en/blog/2015/designs-for-guixsd/
